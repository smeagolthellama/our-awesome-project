CREATE TABLE users (
    id INT(6) AUTO_INCREMENT PRIMARY KEY NOT NULL,
    family_name VARCHAR(255) NOT NULL,
    given_name VARCHAR(255) NOT NULL,
    password VARCHAR(64) NOT NULL,
    email VARCHAR(100) NOT NULL,
    reset_key VARCHAR(64) NOT NULL,
    last_login TIME NOT NULL
);

CREATE TABLE snippets (
    url_slug VARCHAR(6) PRIMARY KEY NOT NULL,
    user_id INT(6) NOT NULL,
    snippet_title TEXT NOT NULL,
    snippet_text TEXT NOT NULL,
    created_at TIME NOT NULL,
    active TINYINT(1) NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id)
);